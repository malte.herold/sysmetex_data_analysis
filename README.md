# Repository for SysMetEx data analysis

## Data files
* Mapped readcounts can be found in `Data/Featurecounts/`
* SampleIDs and accessions can be found in `Tables/`
* References and annotations in `Data/References/`
* [multiQC](https://multiqc.info/) html reports in `multiqc_reports/`


## RNAseq pipeline

The following tools were used:

* Bowtie-2 v2.3.2
* Trimmomatic v0.32
* subread package v1.5.1
* DESeq2 package v1.16.1

### Reusability

The workflow was implemented in [snakemake](https://academic.oup.com/bioinformatics/article/28/19/2520/290322) and was run a the gaia-cluster in the Luxembourgish [HPC infrastructure](https://hpc.uni.lu/).

Snakemake should be installed to rerun the workflow.

To reuse the workflow, dependencies have to be accessible, on the gaia-cluster [this script](preload_modules.sh) was used to load dependencies.

Also in the [rule for running trimmmomatic](rules/trimming.rules) the path to the adapter database has to be changed (l.14) accordingly.


