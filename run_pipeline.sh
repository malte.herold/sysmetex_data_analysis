#!/bin/bash -l


source /work/users/mherold/REPOS/RNAseq_sysmetex_rna/preload_modules.sh

export OUTPUTDIR='/work/users/mherold/SysMetEx_RNAseq_leaching/Analysis'
export SAMPLES=$(\ls -d $OUTPUTDIR/Raw/* | parallel "echo {/}")
#export SAMPLES="P3251_101"
export REFERENCES='/work/users/mherold/REPOS/SysMetEx_ReferenceGenomes/Leptospirillum_ferriphilum/lf_chrm.fna /work/users/mherold/REPOS/SysMetEx_ReferenceGenomes/Acidithiobacillus_caldus/GCF_000175575.2_ASM17557v2_genomic.fna /work/users/mherold/REPOS/SysMetEx_ReferenceGenomes/Sulfobacillus_thermosulfidooxidans/2506210005.fna'
GFFDIR="/work/users/mherold/SysMetEx_RNAseq/sysmetex_rna/Analysis/References"
export GFFS="/work/users/mherold/REPOS/SysMetEx_ReferenceGenomes/Leptospirillum_ferriphilum/lf_chrm.gff $GFFDIR/GCF_000175575.2_ASM17557v2_genomic.gff $GFFDIR/2506210005.gff"

snakemake -j12 -p
snakemake --dag | dot -T png > dag.png
snakemake --rulegraph | dot -T png > rulegraph.png
